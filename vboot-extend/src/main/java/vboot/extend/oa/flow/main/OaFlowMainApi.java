package vboot.extend.oa.flow.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.PageData;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.module.bpm.task.main.BpmTaskMainService;

@RestController
@RequestMapping("oa/flow/main")
@Api(tags = {"OA办公-流程实例"})
public class OaFlowMainApi {

    @GetMapping
    @ApiOperation("查询流程实例分页")
    public R get(String name) {
        Sqler sqler = new Sqler("oa_flow_main");
        sqler.addInnerJoin("p.name temna","oa_flow_temp p", "p.id=t.temid");
        sqler.addLike("t.name", name);
        sqler.addSelect("t.state");
        PageData pageData = service.findPageData(sqler);
        taskMainService.findCurrentExmen(pageData.getItems());
        return R.ok(pageData);
    }


    @GetMapping("one/{id}")
    @ApiOperation("查询流程实例详情")
    public R getOne(@PathVariable String id) {
        OaFlowMain main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增流程实例")
    public R post(@RequestBody OaFlowMain main) throws Exception {
//        service.insert1(main);
//        service.insert2(main);
        service.insertx(main);
        return R.ok();
    }

    @PutMapping
    @ApiOperation("修改流程实例")
    public R put(@RequestBody OaFlowMain main) throws Exception {
        service.updatex(main);
        return R.ok();
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除流程实例")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private BpmTaskMainService taskMainService;

    @Autowired
    private OaFlowMainService service;

}
