package vboot.extend.oa.flow.temp;

import vboot.core.common.mvc.entity.BaseMainEntity;
import lombok.Getter;
import lombok.Setter;
import vboot.extend.oa.flow.cate.OaFlowCate;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class OaFlowTemp extends BaseMainEntity {

    private Integer ornum;

    private String protd;//全局流程模板ID

    @Lob
    private String vform;

    @Transient
    private String prxml;

    @ManyToOne
    @JoinColumn(name = "catid")
    private OaFlowCate cate;//流程分类

}