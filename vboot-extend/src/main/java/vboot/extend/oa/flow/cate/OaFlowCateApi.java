package vboot.extend.oa.flow.cate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("oa/flow/cate")
@Api(tags = {"OA办公-流程分类"})
public class OaFlowCateApi {

    private String table = "oa_flow_cate";

    @GetMapping
    @ApiOperation("查询流程分类分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes");
        sqler.addLeftJoin("c.name panam","oa_flow_cate c","c.id=t.pid");
        sqler.addOrder("t.ornum");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询流程分类详情")
    public R getOne(@PathVariable String id) {
        OaFlowCate cate = service.findOne(id);
        return R.ok(cate);
    }

    @GetMapping("tree")
    @ApiOperation("查询流程分类树")
    public R getTree(String name) {
        List<Ztree> list = service.findTreeList(table,name);
        return R.ok(list);
    }

    @PostMapping
    @ApiOperation("新增流程分类")
    public R post(@RequestBody OaFlowCate cate) {
        return R.ok(service.insert(cate));
    }

    @PutMapping
    @ApiOperation("修改流程分类")
    public R put(@RequestBody OaFlowCate cate) {
        return R.ok(service.update(cate));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除流程分类")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private OaFlowCateService service;

}
