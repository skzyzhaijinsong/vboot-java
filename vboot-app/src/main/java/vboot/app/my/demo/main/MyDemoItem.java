package vboot.app.my.demo.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

//主记录OneToMany
@Data
@Entity
@ApiModel("DEMO明细行")
public class MyDemoItem {
    @Id
    @Column(length = 36)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("姓名")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("手机号")
    private String monum;

    @Column(length = 32)
    @ApiModelProperty("性别")
    private String gender;

    @Column(length = 64)
    @ApiModelProperty("职务")
    private String post;

    @Column(length = 32)
    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("备注")
    private String notes;

    @ApiModelProperty("排序号")
    private Integer ornum;
}
