package vboot.app.my.demo.cate;

import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.Date;

@Getter
@Setter
public class MyDemoCateVo extends Ztree {

    private Date crtim;

    private String crman;

    private Date uptim;

    private String upman;

    private String notes;

}
