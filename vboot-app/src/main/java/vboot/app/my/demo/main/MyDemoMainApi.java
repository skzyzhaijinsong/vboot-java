package vboot.app.my.demo.main;

import cn.hutool.core.util.StrUtil;
import com.taobao.api.ApiException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.PageData;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.module.ass.num.main.AssNumMainService;
import vboot.core.module.bpm.task.main.BpmTaskMainService;
import vboot.core.module.mon.log.oper.Oplog;

@RestController
@RequestMapping("my/demo/main")
@Api(tags = {"DEMO管理"})
public class MyDemoMainApi {

    @Oplog("查询DEMO分页")
    @GetMapping
    @ApiOperation("查询DEMO分页")
    public R get(String name) {
        Sqler sqler = new Sqler("my_demo_main");
        sqler.addLike("t.name", name);
        sqler.addLeftJoin("c.name catna,c.fomod type","my_demo_cate c","c.id=t.catid");
        sqler.addSelect("t.state,t.senum,t.notes");
        PageData pageData = service.findPageData(sqler);
        taskMainService.findCurrentExmen(pageData.getItems());
        return R.ok(pageData);
    }

    @Oplog("查询DEMO详情")
    @GetMapping("one/{id}")
    @ApiOperation("查询DEMO详情")
    public R getOne(@PathVariable String id) throws ApiException {
        MyDemoMain main = service.findOne(id);
        return R.ok(main);
    }

    @Oplog("新增DEMO")
    @PostMapping
    @ApiOperation("新增DEMO")
    public R post(@RequestBody MyDemoMain main) throws Exception {
        main.setSenum(numService.getNum("DEMO"));//设置供应商流水号
        if(StrUtil.isBlank(main.getProtd())){
            main.setState("30");
            service.insert(main);
        }else{
            service.insertx(main);
        }
        return R.ok();
    }

    @Oplog("更新DEMO")
    @PutMapping
    @ApiOperation("更新DEMO")
    public R put(@RequestBody MyDemoMain main) throws Exception {
        if(StrUtil.isBlank(main.getProtd())){
            service.update(main);
        }else{
            service.updatex(main);
        }
        return R.ok();
    }

    @Oplog("删除DEMO")
    @DeleteMapping("{ids}")
    @ApiOperation("删除DEMO")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private BpmTaskMainService taskMainService;

    @Autowired
    private MyDemoMainService service;

    @Autowired
    private AssNumMainService numService;

}
