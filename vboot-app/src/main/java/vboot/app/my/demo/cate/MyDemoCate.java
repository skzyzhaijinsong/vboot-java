package vboot.app.my.demo.cate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseCateEntity;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel("DEMO分类")
public class MyDemoCate extends BaseCateEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "pid")
    @ApiModelProperty("父类别")
    private MyDemoCate parent;

    @Column(length = 32)
    private String fomod;//表单形式

    private Boolean prtag;//是否有流程

    @Column(length = 32)
    private String protd;//全局流程模板ID

    @Transient
    private String prxml;//流程XML

    @Lob
    private String fjson;//fields 表单字段

    @Lob
    private String tjson;//tabs 表单页签

    @Lob
    private String ljson;//links 表单联动

    @ManyToMany
    @JoinTable(name = "my_demo_cate_reman", joinColumns = {@JoinColumn(name = "mid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("数据可阅读者")
    private List<SysOrg> remen;

    @ManyToMany
    @JoinTable(name = "my_demo_cate_usman", joinColumns = {@JoinColumn(name = "mid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("分类可使用者")
    private List<SysOrg> usmen;

}
