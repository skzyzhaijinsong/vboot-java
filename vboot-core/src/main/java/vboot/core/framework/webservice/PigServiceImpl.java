package vboot.core.framework.webservice;


import org.springframework.stereotype.Component;

import javax.jws.WebService;

@Component
@AutoPublish(publishAddress = "/pig")
@WebService(serviceName = "PigService", // 与接口中指定的name一致
        targetNamespace = "http://service.sbmpservice.usts.edu.cn", // 与接口中的命名空间一致,一般是接口的包名倒
        endpointInterface = "vboot.core.framework.webservice.PigService"// 接口地址
)
public class PigServiceImpl implements PigService {


    @Override
    public int addUser(){
        System.out.println("addUser");
        return 2;
    }

}

