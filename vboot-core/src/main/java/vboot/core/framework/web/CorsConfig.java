package vboot.core.framework.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.ContentVersionStrategy;
import org.springframework.web.servlet.resource.VersionResourceResolver;

@Configuration
public class CorsConfig {
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
//                        .allowedHeaders("Authorization")
                        .allowedOrigins("*")
                        .allowCredentials(true)
                        .allowedMethods("GET", "POST", "DELETE", "PUT", "PATCH")
                        .maxAge(3600);
            }


//            @Override//这个在security那里配置了
//            public void addResourceHandlers(ResourceHandlerRegistry registry) {
////                VersionResourceResolver versionResourceResolver = new VersionResourceResolver()
////                        .addVersionStrategy(new ContentVersionStrategy(), "/**");
////                registry.addResourceHandler("static/**")
//////                .addResourceLocations("classpath:/static/")
////                        .setCachePeriod(2592000).resourceChain(true)
////                        .addResolver(versionResourceResolver);
//                registry.addResourceHandler("/gen/file/**")
//                        .addResourceLocations("file:C:\\vboot\\att\\");
//            }
        };
    }
}