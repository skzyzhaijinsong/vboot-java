package vboot.core.framework.security.pojo;

import lombok.Data;

@Data
public class UserVo {

    private String id;//用户ID

    private String name;//用户姓名

    private String usnam;//用户名

    private String monum;//手机号

    private String label;//账号标签

    private String type;//账号类型

    private String depid;//部门id，协同用户则是公司id

    private String depna;//部门名称，协同用户则是公司名称

    private String relog;//前台通知是否查看过的标记

    private String avimg;//头像

}
