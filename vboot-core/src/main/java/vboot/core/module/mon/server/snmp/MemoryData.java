package vboot.core.module.mon.server.snmp;

import lombok.Data;

@Data
public class MemoryData {
    private String address;
    private String totalSize;
    private String userdSize;
    private String unit;
}
