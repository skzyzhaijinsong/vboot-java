package vboot.core.module.mon.job.log;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MonJobLogRepo extends JpaRepository<MonJobLog,String> {

}

