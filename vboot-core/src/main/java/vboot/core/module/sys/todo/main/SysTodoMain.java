package vboot.core.module.sys.todo.main;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@ApiModel("待办信息")
public class SysTodoMain {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 256)
    @ApiModelProperty("主题")
    private String name;

    @Column(length = 8)
    @ApiModelProperty("类型")
    private String type;

    @Column(length = 8)
    @ApiModelProperty("紧急度")
    private String grade;

    @Column(length = 128)
    @ApiModelProperty("模型分类")
    private String modca;

    @Column(length = 32)
    @ApiModelProperty("模型ID")
    private String modid;

    @Column(length = 512)
    @ApiModelProperty("链接")
    private String link;

    @ApiModelProperty("备注")
    private String notes;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @Column(updatable = false)
    @ApiModelProperty("创建人")
    private String crman;

//    @ManyToMany
//    @JoinTable(name = "sys_todo_user", joinColumns = {@JoinColumn(name = "tid")},
//            inverseJoinColumns = {@JoinColumn(name = "uid")})
//    private List<SysOrg> tamen;

}
