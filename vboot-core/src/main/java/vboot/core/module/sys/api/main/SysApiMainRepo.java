package vboot.core.module.sys.api.main;

import org.springframework.data.jpa.repository.JpaRepository;


public interface SysApiMainRepo extends JpaRepository<SysApiMain,String> {
}

