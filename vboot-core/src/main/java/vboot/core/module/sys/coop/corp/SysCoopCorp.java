package vboot.core.module.sys.coop.corp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@Getter
@Setter
@ApiModel("外部公司")
public class SysCoopCorp extends BaseMainEntity {

    @ApiModelProperty("排序号")
    private Integer ornum;

    @Column(length = 32)
    @ApiModelProperty("分类ID")
    private String catid;

    @Column(length = 32)
    @ApiModelProperty("状态")
    private String state;

    @Column(length = 32)
    @ApiModelProperty("流水号")
    private String senum;

    @Column(length = 32)
    @ApiModelProperty("销售机构ID")
    private String braid;

    @Transient
    private String brana;

    //地址选择示例，可根据实际情况存想要的字段
    //region-----地址相关信息-----
    @ApiModelProperty("完整地址")
    private String addre;

    @Column(length = 64)
    @ApiModelProperty("省市区")
    private String adreg;

    @Column(length = 128)
    @ApiModelProperty("省市区以外的详细信息")
    private String addet;

    @Column(length = 32)
    @ApiModelProperty("经纬度")
    private String adcoo;

    @Column(length = 32)
    @ApiModelProperty("省")
    private String adpro;

    @Column(length = 32)
    @ApiModelProperty("市")
    private String adcit;

    @Column(length = 32)
    @ApiModelProperty("区")
    private String addis;
    //endregion

    @Column(length = 32)
    @ApiModelProperty("税务登记号")
    private String renum;

    @Column(length = 32)
    @ApiModelProperty("法人代表")
    private String leman;

    @Column(length = 32)
    @ApiModelProperty("公司电话")
    public String cotel;

    @Column(length = 32)
    @ApiModelProperty("公司传真")
    public String cofax;

    @Column(length = 32)
    @ApiModelProperty("成立日期")
    public String redat;

    @Column(length = 32)
    @ApiModelProperty("注册资金（万）")
    public String recap;

    @Column(length = 32)
    @ApiModelProperty("邮编")
    public String cozip;

    @Column(length = 32)
    @ApiModelProperty("公司邮箱")
    public String comai;

    @ApiModelProperty("关联企业")
    public String licos;

    @Column(length = 64)
    @ApiModelProperty("备用字段1")
    public String key1;

    @Column(length = 64)
    @ApiModelProperty("备用字段1")
    public String key2;

    @Column(length = 64)
    @ApiModelProperty("备用字段2")
    public String key3;

    @Column(length = 64)
    @ApiModelProperty("备用字段3")
    public String key4;

    @Column(length = 64)
    @ApiModelProperty("备用字段4")
    public String key5;

}