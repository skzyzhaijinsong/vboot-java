package vboot.core.module.sys.portal.menu;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@ApiModel("门户菜单")
public class SysPortalMenu {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("父ID")
    private String pid;

    @Transient
    @ApiModelProperty("父菜单名称")
    private String pname;

    @Column(length = 32)
    @ApiModelProperty("门户id")
    private String porid;

    @Transient
    @ApiModelProperty("子菜单列表")
    private List<SysPortalMenu> children = new ArrayList<>();

    @Column(length = 32)
    @ApiModelProperty("类型 C:目录，M：菜单，B：按钮")
    private String type;

    @Column(length = 64)
    @ApiModelProperty("名称")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("代码")
    private String code;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @Column(length = 64)
    @ApiModelProperty("图标")
    private String icon;

    @Column(length = 64)
    @ApiModelProperty("路由地址")
    private String path;

    @Column(length = 64)
    @ApiModelProperty("组件路径")
    private String comp;

    @Column(length = 64)
    @ApiModelProperty("权限标识")
    private String perm;

    @Column(length = 64)
    @ApiModelProperty("跳转")
    private String redirect;

    @ApiModelProperty("外链标记")
    private Boolean extag;

    @ApiModelProperty("iframe标记")
    private Boolean iftag;

    @ApiModelProperty("缓存标记")
    private Boolean catag;

    @ApiModelProperty("是否显示")
    private Boolean shtag;

    @ApiModelProperty("是否可用")
    private Boolean avtag;

    @ApiModelProperty("是否固定")
    private Boolean aftag=false;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ApiModelProperty("更新时间")
    private Date uptim;

    @ApiModelProperty("API接口")
    private String api;

}