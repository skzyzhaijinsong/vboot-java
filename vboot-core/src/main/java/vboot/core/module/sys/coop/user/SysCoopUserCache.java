package vboot.core.module.sys.coop.user;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
@Data
public class SysCoopUserCache {
    @Id
    @Column(length = 32)
    private String id;

    @Column(length = 2000)
    private String conds;//组织架构集

    @Column(length = 2000)
    private String portals;//前台门户缓存

    @Column(length = 2000)
    private String perms;//后台所有权限集

    @Lob
    private String menus;//前台菜单缓存

    @Lob
    private String btns;//前台按钮缓存

}