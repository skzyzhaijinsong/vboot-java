package vboot.core.module.sys.coop.cate;

import org.springframework.data.jpa.repository.JpaRepository;
import vboot.core.module.sys.org.group.SysOrgGroupCate;

public interface SysCoopCateRepo extends JpaRepository<SysCoopCate,String> {

}
