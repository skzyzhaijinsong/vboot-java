package vboot.core.module.gen.coop;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.pojo.ZidName;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("gen/coop")
public class GenCoopApi {

    @GetMapping("tree")
    public R getTree(String name) {
        String sql = "select id,pid,name from sys_coop_cate " +
                "union all select id,catid as pid,name from sys_coop_corp";
        List<Ztree> list = jdbcDao.findTreeList(sql);
        return R.ok(list);
    }

    @GetMapping("list")
    public R getList(String pid, Integer type) {
        if (StrUtil.isBlank(pid) && (type & 1) != 0) {
            String sql = "select id,name from sys_coop_cate where pid is null order by ornum";
            return R.ok(jdbcDao.findIdNameList(sql));
        }
        List<ZidName> list=new ArrayList<>();
        if ((type & 1) != 0) {
            String sql = "select id,name from sys_coop_cate where pid=? order by ornum";
            list.addAll(jdbcDao.findIdNameList(sql, pid));
        }
        if ((type & 2) != 0) {
            String sql = "select id,name from sys_coop_corp where catid=?";
            list.addAll(jdbcDao.findIdNameList(sql, pid));
        }
        if ((type & 4) != 0) {
            String sql = "select id,name from sys_coop_user where corid=? order by ornum";
            list.addAll(jdbcDao.findIdNameList(sql, pid));
        }
        return R.ok(list);
    }


    @Autowired
    private JdbcDao jdbcDao;


}
