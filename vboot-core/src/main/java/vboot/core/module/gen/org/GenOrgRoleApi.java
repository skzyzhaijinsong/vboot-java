package vboot.core.module.gen.org;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.List;

@RestController
@RequestMapping("gen/org/role")
public class GenOrgRoleApi {

    //查询角色树
    @GetMapping("tree")
    public R getTree(String name) {
        Sqler sqler = new Sqler("sys_org_role_tree");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        sqler.addSelect("null pid");
        List<Ztree> list = jdbcDao.findTreeList(sqler);
        return R.ok(list);
    }

    //查询角色
    @GetMapping("list")
    public R getList(String treid,String name) {
        Sqler sqler = new Sqler("sys_org_role");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addEqual("t.treid",treid);
        return R.ok(jdbcDao.findMapList(sqler));
    }


    @Autowired
    private JdbcDao jdbcDao;


//    @Autowired
//    private SysOrgDeptService deptService;

}
