package vboot.core.module.ass.oss.file;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AssOssFileRepo extends JpaRepository<AssOssFile,String> {


    AssOssFile findByMd5(String md5);

}
