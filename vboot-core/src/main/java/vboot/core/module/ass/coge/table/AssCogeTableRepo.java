package vboot.core.module.ass.coge.table;

import org.springframework.data.jpa.repository.JpaRepository;
import vboot.core.module.sys.org.group.SysOrgGroup;

import java.util.List;


public interface AssCogeTableRepo extends JpaRepository<AssCogeTable,String> {

    AssCogeTable findByName(String name);

}

