package vboot.core.module.ass.addr.pojo;

import lombok.Data;

@Data
public class Apath {
    private int distance;

    private int duration;

    private String strategy;

    private int tolls;



}
