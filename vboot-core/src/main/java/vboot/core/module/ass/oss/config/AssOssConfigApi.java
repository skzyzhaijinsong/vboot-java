package vboot.core.module.ass.oss.config;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;

@RestController
@RequestMapping("ass/oss/config")
@Api(tags = {"存储配置"})
public class AssOssConfigApi {

    @GetMapping
    @ApiOperation("查询存储配置分页")
    public R get(String name) {
        Sqler sqler = new Sqler("t.id,t.config_key,t.access_key,t.secretKey", "ass_oss_config");
        sqler.addLike("t.config_key", name);
        sqler.addSelect("t.endpoint,t.bucket_name,t.prefix,t.status");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    @Autowired
    private JdbcDao jdbcDao;


}
