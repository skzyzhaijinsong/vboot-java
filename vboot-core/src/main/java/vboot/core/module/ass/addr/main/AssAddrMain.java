package vboot.core.module.ass.addr.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Getter
@Setter
@ApiModel("地址信息")
public class AssAddrMain extends BaseEntity {
    @Column(length = 32)
    @ApiModelProperty("父ID")
    private String pid;

    @Column(length = 32)
    @ApiModelProperty("坐标")
    private String cecoo;

    @Column(length = 32)
    @ApiModelProperty("类型")
    private String type;

    @Column(length = 32)
    @ApiModelProperty("代码")
    private String code;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ApiModelProperty("更新时间")
    private Date uptim;

    @ApiModelProperty("备注")
    private String notes;

}
