package vboot.core.module.ass.oss.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import vboot.core.common.utils.ruoyi.JsonUtils;
import vboot.core.common.utils.ruoyi.RedisUtils;
import vboot.core.module.ass.oss.root.OssConstant;
import vboot.core.module.ass.oss.root.OssFactory;

import java.util.List;

/**
 * 对象存储配置Service业务层处理
 *
 * @author Lion Li
 * @author 孤舟烟雨
 * @date 2021-08-13
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class AssOssConfigService  {

    @Value("${oss.configkey}")
    private String ConfigKey;

    @Value("${oss.accesskey}")
    private String AccessKey;

    @Value("${oss.secretkey}")
    private String SecretKey;

    @Value("${oss.bucket}")
    private String BucketName;

    @Value("${oss.endpoint}")
    private String Endpoint;

    @Value("${oss.ishttps}")
    private String IsHttps;

    @Value("${oss.region}")
    private String Region;


    //    private final SysOssConfigMapper baseMapper;
    @Autowired
    private AssOssConfigRepo repo;

    /**
     * 项目启动时，初始化参数到缓存，加载配置类
     */
    public void init() {
        List<AssOssConfig> list = repo.findAll();
        AssOssConfig assOssConfig=new AssOssConfig();
        assOssConfig.setConfigKey(ConfigKey);
        assOssConfig.setAccessKey(AccessKey);
        assOssConfig.setSecretKey(SecretKey);
        assOssConfig.setBucketName(BucketName);
        assOssConfig.setEndpoint(Endpoint);
        assOssConfig.setIsHttps(IsHttps);
        assOssConfig.setRegion(Region);
        assOssConfig.setStatus("0");
        assOssConfig.setId("111");
        list.add(assOssConfig);
        // 加载OSS初始化配置
        for (AssOssConfig config : list) {
            String configKey = config.getConfigKey();
            if ("0".equals(config.getStatus())) {
                RedisUtils.setCacheObject(OssConstant.CACHE_CONFIG_KEY, configKey);
            }
            setConfigCache(true, config);
        }
        // 初始化OSS工厂
        OssFactory.init();
    }

    //    @Override
//    public SysOssConfigVo queryById(Long ossConfigId) {
//        return baseMapper.selectVoById(ossConfigId);
//    }
//
//    @Override
//    public TableDataInfo<SysOssConfigVo> queryPageList(SysOssConfigBo bo, PageQuery pageQuery) {
//        LambdaQueryWrapper<SysOssConfig> lqw = buildQueryWrapper(bo);
//        Page<SysOssConfigVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
//        return TableDataInfo.build(result);
//    }
//
//
//    private LambdaQueryWrapper<SysOssConfig> buildQueryWrapper(SysOssConfigBo bo) {
//        LambdaQueryWrapper<SysOssConfig> lqw = Wrappers.lambdaQuery();
//        lqw.eq(StringUtils.isNotBlank(bo.getConfigKey()), SysOssConfig::getConfigKey, bo.getConfigKey());
//        lqw.like(StringUtils.isNotBlank(bo.getBucketName()), SysOssConfig::getBucketName, bo.getBucketName());
//        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), SysOssConfig::getStatus, bo.getStatus());
//        return lqw;
//    }
//
//    @Override
//    public Boolean insertByBo(SysOssConfigBo bo) {
//        SysOssConfig config = BeanUtil.toBean(bo, SysOssConfig.class);
//        validEntityBeforeSave(config);
//        return setConfigCache(baseMapper.insert(config) > 0, config);
//    }
//
//    @Override
//    public Boolean updateByBo(SysOssConfigBo bo) {
//        SysOssConfig config = BeanUtil.toBean(bo, SysOssConfig.class);
//        validEntityBeforeSave(config);
//        LambdaUpdateWrapper<SysOssConfig> luw = new LambdaUpdateWrapper<>();
//        luw.set(ObjectUtil.isNull(config.getPrefix()), SysOssConfig::getPrefix, "");
//        luw.set(ObjectUtil.isNull(config.getRegion()), SysOssConfig::getRegion, "");
//        luw.set(ObjectUtil.isNull(config.getExt1()), SysOssConfig::getExt1, "");
//        luw.set(ObjectUtil.isNull(config.getRemark()), SysOssConfig::getRemark, "");
//        luw.eq(SysOssConfig::getOssConfigId, config.getOssConfigId());
//        return setConfigCache(baseMapper.update(config, luw) > 0, config);
//    }
//
//    /**
//     * 保存前的数据校验
//     */
//    private void validEntityBeforeSave(SysOssConfig entity) {
//        if (StringUtils.isNotEmpty(entity.getConfigKey())
//            && UserConstants.NOT_UNIQUE.equals(checkConfigKeyUnique(entity))) {
//            throw new ServiceException("操作配置'" + entity.getConfigKey() + "'失败, 配置key已存在!");
//        }
//    }
//
//    @Override
//    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
//        if (isValid) {
//            if (CollUtil.containsAny(ids, OssConstant.SYSTEM_DATA_IDS)) {
//                throw new ServiceException("系统内置, 不可删除!");
//            }
//        }
//        List<SysOssConfig> list = Lists.newArrayList();
//        for (Long configId : ids) {
//            SysOssConfig config = baseMapper.selectById(configId);
//            list.add(config);
//        }
//        boolean flag = baseMapper.deleteBatchIds(ids) > 0;
//        if (flag) {
//            list.stream().forEach(sysOssConfig -> {
//                RedisUtils.deleteObject(getCacheKey(sysOssConfig.getConfigKey()));
//            });
//        }
//        return flag;
//    }
//
//    /**
//     * 判断configKey是否唯一
//     */
//    private String checkConfigKeyUnique(SysOssConfig sysOssConfig) {
//        long ossConfigId = ObjectUtil.isNull(sysOssConfig.getOssConfigId()) ? -1L : sysOssConfig.getOssConfigId();
//        SysOssConfig info = baseMapper.selectOne(new LambdaQueryWrapper<SysOssConfig>()
//            .select(SysOssConfig::getOssConfigId, SysOssConfig::getConfigKey)
//            .eq(SysOssConfig::getConfigKey, sysOssConfig.getConfigKey()));
//        if (ObjectUtil.isNotNull(info) && info.getOssConfigId() != ossConfigId) {
//            return UserConstants.NOT_UNIQUE;
//        }
//        return UserConstants.UNIQUE;
//    }
//
//    /**
//     * 启用禁用状态
//     */
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public int updateOssConfigStatus(SysOssConfigBo bo) {
//        SysOssConfig sysOssConfig = BeanUtil.toBean(bo, SysOssConfig.class);
//        int row = baseMapper.update(null, new LambdaUpdateWrapper<SysOssConfig>()
//            .set(SysOssConfig::getStatus, "1"));
//        row += baseMapper.updateById(sysOssConfig);
//        if (row > 0) {
//            RedisUtils.setCacheObject(OssConstant.CACHE_CONFIG_KEY, sysOssConfig.getConfigKey());
//        }
//        return row;
//    }
//
//    /**
//     * 设置cache key
//     *
//     * @param configKey 参数键
//     * @return 缓存键key
//     */
    private String getCacheKey(String configKey) {
        return OssConstant.SYS_OSS_KEY + configKey;
    }
    //
//    /**
//     * 如果操作成功 则更新缓存
//     *
//     * @param flag   操作状态
//     * @param config 配置
//     * @return 返回操作状态
//     */
    private boolean setConfigCache(boolean flag, AssOssConfig config) {
        if (flag) {
            RedisUtils.setCacheObject(
                    getCacheKey(config.getConfigKey()),
                    JsonUtils.toJsonString(config));
            RedisUtils.publish(OssConstant.CACHE_CONFIG_KEY, config.getConfigKey(), msg -> {
                log.info("发布刷新OSS配置 => " + msg);
            });
        }
        return flag;
    }
}
