package vboot.core.module.ass.oss.main;

import com.alibaba.excel.annotation.ExcelIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import vboot.core.module.sys.org.root.SysOrg;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@ApiModel("OSS存储引用")
public class AssOssMain {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("文件名称")
    private String name;

    @ApiModelProperty("类型（后缀）")
    @Column(length = 32)
    private String type;

    @ApiModelProperty("文件ID")
    @Column(length = 32)
    private String filid;

    @ApiModelProperty("业务ID")
    @Column(length = 32)
    private String busid;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ManyToOne
    @JoinColumn(name = "crmid", updatable = false)
    @ExcelIgnore
    @ApiModelProperty("创建人")
    protected SysOrg crman;



}
