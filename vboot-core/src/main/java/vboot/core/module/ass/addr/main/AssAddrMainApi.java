package vboot.core.module.ass.addr.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.utils.file.ExcelUtil;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("ass/addr/main")
@Api(tags = {"地址信息"})
public class AssAddrMainApi {

    @GetMapping
    @ApiOperation("查询地址分页")
    public R get(String name) {
        Sqler sqler = new Sqler("ass_addr_main");
        sqler.addLike("t.name", name);
        sqler.addLeftJoin("t2.name as p2name","ass_addr_main t2","t2.id=t.pid");
        sqler.addLeftJoin("t3.name as p3name","ass_addr_main t3","t3.id=t2.pid");
        sqler.addLeftJoin("t4.name as p4name","ass_addr_main t4","t4.id=t3.pid");
        sqler.addSelect("t.type,t.crtim,t.uptim,t.cecoo");
        sqler.addOrder("t.id");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询地址详情")
    public R getOne(@PathVariable String id) {
        AssAddrMain main = service.findOne(id);
        return R.ok(main);
    }

    @PutMapping("addrupdate")
    @ApiOperation("通过高德地图接口更新地址信息")
    public R addrupdate() {
        addrHandle.addrInit();
        return R.ok();
    }

    @GetMapping("exp")
    @ApiOperation("导出地址清单")
    public void getExp(HttpServletResponse response) throws Exception {
        List<AssAddrMain> list = service.findAll();
        ExcelUtil.exportExcel(list, "字典数据", AssAddrMain.class, response);
    }


    @PostMapping
    @ApiOperation("新增地址")
    public R post(@RequestBody AssAddrMain main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("更新地址")
    public R put(@RequestBody AssAddrMain main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除地址")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private AssAddrMainService service;

    @Autowired
    private AssAddrMainHand addrHandle;
}
