package vboot.core.module.bpm.audit.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@ApiModel("流程评审附件")
public class BpmAuditAtt
{
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @ApiModelProperty("附件名称")
    private String name;

    @ApiModelProperty("附件大小")
    private String zsize;

    @ApiModelProperty("存储地址")
    private String path;

    @ApiModelProperty("文件ID")
    @Column(length = 32)
    private String filid;

    @ApiModelProperty("评审ID")
    @Column(length = 32)
    private String audid;

    @ApiModelProperty("排序号")
    private Integer ornum;

}
