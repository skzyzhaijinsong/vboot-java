package vboot.core.module.bpm.proc.cond;


import org.springframework.data.jpa.repository.JpaRepository;
import vboot.core.module.bpm.proc.main.BpmProcMain;

public interface BpmProcCondRepo extends JpaRepository<BpmProcCond,String> {


}