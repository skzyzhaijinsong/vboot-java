package vboot.core.module.bpm.proc.main;

import lombok.Data;

import java.util.List;

@Data
public class Zbpm {
    private String todid;//待办ID

    private String modty;//业务模型类型

    private String temid;//流程模板ID

    private String proid;//流程实例ID

    private String prona;//流程实例名称

    private String nodid;//当前节点ID

    private String facno;//当前节点编号

    private String facna;//当前节点名称

    private String tarno;//目标节点编号

    private String tarna;//目标节点名称

    private Boolean retag=true;//驳回标记，驳回的节点通过后直接返回本节点

    private String bacid;//驳回后的流程重新提交时的bpm_proc_param的id

    private Boolean tutag=false;//转办标记，流程重新流经本节点时，直接由转办人员处理

    private String tumid;//转办人员ID

    private Boolean cotag=false;//沟通标记，是否显示意见

    private String coids;//沟通人员IDS

    private String ccids;//取消沟通的task_id

    private String tasid;//任务ID

    private String tasty;//任务类型

    private String opnot;//操作：处理意见

    private String opurg;//操作：紧急程度

    private String opkey;//操作key:pass, reject

    private String opinf;//操作名称:通过，驳回到谁，沟通谁

    private String chxml;//优化过的vboot可解析的的xml

    private String haman;//当前处理人ID

    private String exman;//应处理人ID

    private String atids;//附件IDS

    public Zbpm() {
    }

    public Zbpm(String proid) {
        this.proid = proid;
    }

    public Zbpm(String proid,String prona) {
        this.proid = proid;
        this.prona = prona;
    }
}
