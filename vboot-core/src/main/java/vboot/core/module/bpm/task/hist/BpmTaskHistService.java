package vboot.core.module.bpm.task.hist;


import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.module.bpm.task.main.BpmTaskMain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional(rollbackFor = Exception.class)
@Service
public class BpmTaskHistService {

    public BpmTaskHist createTask(BpmTaskMain mainTask) {
        BpmTaskHist histTask = new BpmTaskHist();
        histTask.setId(mainTask.getId());
        histTask.setProid(mainTask.getProid());
        histTask.setState("20");
        histTask.setExman(mainTask.getExman());
        histTask.setNodid(mainTask.getNodid());
        histTask.setType(mainTask.getType());
        return repo.save(histTask);
    }

    public List<BpmTaskHist> createTaskList(List<BpmTaskMain> mainTaskList) {
        List<BpmTaskHist> list=new ArrayList<>();
        for (BpmTaskMain mainTask : mainTaskList) {
            if(mainTask.getActag()){
                BpmTaskHist histTask = new BpmTaskHist();
                histTask.setId(mainTask.getId());
                histTask.setProid(mainTask.getProid());
                histTask.setState("20");
                histTask.setExman(mainTask.getExman());
                histTask.setNodid(mainTask.getNodid());
                histTask.setType(mainTask.getType());
                list.add(repo.save(histTask));
            }
        }
        return list;
    }


    @Transactional(readOnly = true)
    public BpmTaskHist findOne(String id) {
        return repo.findById(id).get();
    }

    public void delete(String id) {
         repo.deleteById(id);
    }

    public void deleteAllByProidNotEnd(String proid) {
        repo.deleteAllByProidAndState(proid,"20");
    }



    @Autowired
    protected JdbcDao jdbcDao;


    @Autowired
    private BpmTaskHistRepo repo;
}
