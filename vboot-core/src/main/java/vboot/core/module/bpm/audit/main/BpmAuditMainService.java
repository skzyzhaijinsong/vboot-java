package vboot.core.module.bpm.audit.main;


import vboot.core.module.bpm.proc.main.Zbpm;
import vboot.core.module.bpm.proc.main.Znode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vboot.core.common.utils.lang.IdUtils;

@Transactional(rollbackFor = Exception.class)
@Service
public class BpmAuditMainService {

    public BpmAuditMain saveDraftAudit(Zbpm zbpm, Znode znode) {
        BpmAuditMain audit = new BpmAuditMain();
        audit.setId(IdUtils.getUID());
        audit.setFacno(znode.getFacno());
        audit.setFacna(znode.getFacna());
        audit.setNodid(znode.getNodid());
        audit.setHaman(zbpm.getHaman());
        audit.setProid(zbpm.getProid());
        audit.setOpnot(zbpm.getOpnot());
        audit.setAtids(zbpm.getAtids());
        audit.setOpkey("dsubmit");
        audit.setOpinf("起草人提交");
        return repo.save(audit);
    }

    public BpmAuditMain saveAudit(Zbpm zbpm) {
        BpmAuditMain audit = new BpmAuditMain();
        audit.setId(IdUtils.getUID());
        audit.setFacno(zbpm.getFacno());
        audit.setFacna(zbpm.getFacna());
        audit.setNodid(zbpm.getNodid());
        audit.setHaman(zbpm.getHaman());
        audit.setProid(zbpm.getProid());
        audit.setOpnot(zbpm.getOpnot());
        audit.setOpkey(zbpm.getOpkey());
        audit.setOpinf(zbpm.getOpinf());
        audit.setTasid(zbpm.getTasid());
        audit.setAtids(zbpm.getAtids());
        return repo.save(audit);
    }

    public BpmAuditMain saveEndAudit(Zbpm zbpm, String nodid) {
        BpmAuditMain audit = new BpmAuditMain();
        audit.setId(IdUtils.getUID());
        audit.setFacno("NE");
        audit.setFacna("结束节点");
        audit.setNodid(nodid);
        audit.setProid(zbpm.getProid());
        audit.setOpkey("end");
        return repo.save(audit);
    }




    @Autowired
    private BpmAuditMainRepo repo;
}
