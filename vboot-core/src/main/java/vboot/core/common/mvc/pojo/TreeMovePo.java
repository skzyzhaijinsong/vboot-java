package vboot.core.common.mvc.pojo;

import lombok.Data;

@Data
public class TreeMovePo {

    private String draid;

    private String droid;

    private String type;

}
