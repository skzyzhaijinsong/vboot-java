package vboot.core.common.mvc.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import io.swagger.annotations.ApiModelProperty;
import vboot.core.module.sys.org.root.SysOrg;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

//主数据Entity基类，提供通用的字段
@MappedSuperclass
@Getter
@Setter
public class BaseMainEntity {
    //----------------------id属性-----------------------
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    protected String id;

    //----------------------权限属性-----------------------
//    @ManyToMany
//    @JoinTable(name = "xxx_viewer", joinColumns = {@JoinColumn(name = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "org")})
//    private List<SysOrg> viewers;//记录可查看者
//
//    @ManyToMany
//    @JoinTable(name = "xxx_editor", joinColumns = {@JoinColumn(name = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "org")})
//    private List<SysOrg> editors;//记录可编辑者


    //----------------------norm标准属性-----------------------
    @Column(length = 126)
    @ApiModelProperty("名称")
    protected String name;

    @ApiModelProperty("可用标记")// 1启用，0禁用
    protected Boolean avtag = true;

    @ManyToOne
    @JoinColumn(name = "crmid", updatable = false)
    @ExcelIgnore
    @ApiModelProperty("创建人")
    protected SysOrg crman;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    protected Date crtim = new Date();

    @ManyToOne
    @JoinColumn(name = "upmid")
    @ExcelIgnore
    @ApiModelProperty("更新人")
    protected SysOrg upman;

    @ApiModelProperty("更新时间")
    protected Date uptim;

    @ApiModelProperty("备注")
    protected String notes;

}
